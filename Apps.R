#
# path <- 'NbaAllSessonTeamStat.csv'
# data <- read.csv(path,sep=';')%>%
#   select(-X...)
# head(data)
# str(data)

#---------------------------------PROCESSING --------------------------------
#-----------------------------------------------------------------------------
#---Gloassary data ---
#
# GP Games Played W Wins L Losses WIN%Win Percentage MIN Minutes PlayedPTSPointsFGMField Goals MadeFGAField Goals AttemptedFG%Field Goal Percentage3PM3 Point Field Goals Made3PA3 Point Field Goals Attempted3P%3 Point Field Goal Percentage FTMFree Throws MadeFTAFree Throws Attempted FT%Free Throw PercentageOREBOffensive ReboundsDREBDefensive Rebounds REB Rebounds AST Assists TOV Turnovers STL Steals BLK Blocks BLKA Blocks Against PF PersonalFouls
# PFDPersonal Fouls Drawn+/-Plus-Minus
#
# c('Games Played',"Win",'Loss','Percent Win','Minutes',
#   'Points','Field')
# Glossary_data <- data.frame(Abreviation =
#                             valeur =   )

options(repos = c(CRAN = "https://cran.rstudio.com"))

source("VAR_ENV.R", local = TRUE, encoding = "UTF8")

# theme <- bs_theme(
#   # Controls the default grayscale palette
#   bg = "#202123", fg = "#B8BCC2",
#   # Controls the accent (e.g., hyperlink, button, etc) colors
#   primary = "#EA80FC", secondary = "#48DAC6",
#   base_font = c("Grandstander", "sans-serif"),
#   code_font = c("Courier", "monospace"),
#   heading_font = "'Helvetica Neue', Helvetica, sans-serif",
#   # Can also add lower-level customization
#   "input-border-color" = "#EA80FC"
# )

ui_statindiv <- source("Statistiques individuelles/UI_StatIndiv.R", local = TRUE, encoding = "UTF8")
GeneralUI <- navbarPage(
  "NBA ANALYTICS",
  # includeCSS("styles.css"),
  tabPanel(
    title = "Home", icon = icon("house"), source("Accueil/ui_accueil.R", local = TRUE, encoding = "UTF8")),
    tabPanel("Team's Performances", icon = icon("sport"), source("Statistiques individuelles/UI_StatIndiv.R", local = TRUE, encoding = "UTF8"))
  # tabPanel("Table")
)
GeneralServer <- function(input, output, session) { 
  source("Statistiques individuelles/Server_StatIndiv.R", local = TRUE, encoding = "UTF8")
}
shinyApp(ui = GeneralUI, server = GeneralServer)

